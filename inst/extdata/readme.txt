Nom Court	Variable	Commentaire variable
FqMsF	Frequence_de_feuilles_avec_symptomes_mildiou__type_sensible_avec_sporulation - 03/08/21	Notation de la fréquence en % de feuilles avec des sympômes de mildiou de type sensible avec sporulati
FqMnF	Frequence_de_feuilles_avec_symptomes_mildiou__type_necrotique_avec_sporulation - 03/08/21	Notation de la fréquence en % de feuilles avec des sympômes de mildiou de type nécrotique (
FqBRF	Frequence_de_feuilles_avec_symptomes_black_rot - 03/08/21	Fréquence de feuilles avec des symptomes de black ro
FqOF	Frequence_de_feuilles_avec_symptomes_oidium - 03/08/21	Fréquence de feuilles avec des symptômes de'oïd
ImMsF	Intensite_moyenne_des_symptomes_mildiou_type_sensible_sur_feuilles - 03/08/21	Notation de l'intensité moyenne de symptômes de mildiou de type sensible sur feuill
ImMnF	Intensite_moyenne_des_symptomes_mildiou_type_necrotique_sur_feuilles - 03/08/21	Notation de l'intensité moyenne des symptomes de mildiou de type nécrotique sans sporulation (HR) observées sur feuil
ImBRF	Intensite_moyenne_des_symptomes_de_black_rot_sur_feuilles - 03/08/21	Notation de l'intensité moyenne de symptômes de black rot sur les feuill
ImOF	Intensite_moyenne_des_symptomes_oidium_sur_feuilles - 03/08/21	Notation de l'intensité moyenne de symptômes d'oïdium sur les feuil
IxMsF	Intensite_maximale_des_symptomes_de_mildiou_de_type_sensible_sur_feuiles - 03/08/21	Notation de l'intensité moyenne de symptômes de mildiou de type sensible sur les feuill
IxMnF	Intensite_maximale_des_symptomes_de_mildiou_de_type_necrotique_sur_feuilles - 03/08/21	Notation de l'intensité moyenne de symptômes de mildiou de type nécrotique (HR) sur les feuil
IxBRF	Intensite_maximale_des_symptomes_de_black_rot_sur_feuilles - 03/08/21	Notation de l'intensité maximale de symptômes de black rot sur les feuill
IxOF	Intensite_maximale_des_symptomes_d_oidium_sur_feuilles - 03/08/21	Notation de l'intensité maximale de symptômes d'oïdium sur les feuil
PcMG	Pourcentage_de_grappes_necrosees_par_le_mildiou - 03/08/21	Notation globale du pourcentage de grappes détruites par le mildio
PcBRG	Pourcentage_de_grappes_avec_black_rot - 03/08/21	Notation globale du pourcentage de grappes ayant du black rot
PcOG	Pourcentage_de_grappes_avec_oidium - 03/08/21	Notation globale du pourcentage de grappes avec des symptomes d'oïdiu
		
