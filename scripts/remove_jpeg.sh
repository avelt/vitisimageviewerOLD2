#!/bin/bash
for file in `ls Symptomes_50025_2021/*jpeg`
do
  name=$( echo $file | cut -f2 -d"/" )
  if grep -q "$name" Liste_photos.txt
  then 
    continue
  else
    echo "Remove Symptomes_50025_2021/$name"
    rm Symptomes_50025_2021/$name
  fi
done