# VitisImageViewer

Lien vers l'application : 

https://shiny-192-168-101-44.vm.openstack.genouest.org/vitisimageviewer/

Pour ajouter de nouvelles données à la table, lancer l'application, et uploader la nouvelle table au format xlsx.

Pour ajouter les nouvelles images correspondantes, il faut uploader un fichier zip contenant toutes les photos. Attention les photos doivent être directement dans le zip et non pas dans un dossier.
