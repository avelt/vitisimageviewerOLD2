- Exemple de visualisations d'image dans une application Shiny : 
https://github.com/khondula/image-viewer

- Pour créer une application Shiny avec Golem :
File -> New_project -> New Directory -> Package for Shiny App using Golem

- Le projet créé a cette structure : 
#> ├── DESCRIPTION 
#> ├── NAMESPACE 
#> ├── R 
#> │   ├── app_config.R 
#> │   ├── app_server.R 
#> │   ├── app_ui.R 
#> │   └── run_app.R 
#> ├── dev 
#> │   ├── 01_start.R 
#> │   ├── 02_dev.R 
#> │   ├── 03_deploy.R 
#> │   └── run_dev.R 
#> ├── inst 
#> │   ├── app 
#> │   │   └── www 
#> │   │       └── favicon.ico 
#> │   └── golem-config.yml 
#> └── man 
#>     └── run_app.Rd


- Modification du fichier DESCRIPTION pour le remplir correctement.

- Pour pusher les modifications, lancer git bash :
cd /c/Users/avelt/Documents/applications_shiny/vitisimageviewer
git add --all
git commit -m ""
git push origin main

- Première chose à faire : lancer le code du script dev/01_start.R -> à lancer une fois au début du projet
Ici je rempli les commandes et je les lance
Pour que les commit fonctionne via Golem, j'ai du lancer les commandes suivantes avec git sous Windows
  git config --global user.name "avelt"
  git config --global user.email "amandine.velt@inrae.fr"

- Une fois dev/01_start.R lancé, pusher les nouveaux fichiers générés puis tester l'application : 
golem::run_dev()

Le projet est correctement initialisé, on peut maintenant passer au développement avec le fichier dev/02_dev.R

- Pour ajouter un package comme dépendance de l'application, ajouter : usethis::use_package("pkg") dans le fichier dev/02_dev.R

- Pour ajouter un module shiny à l'application, ajouter : golem::add_module( name = "my_first_module" ) dans le fichier dev/02_dev.R -> cela va créer un nouveau fichier contenant un module vide pré-rempli qu'on peut ensuite remplir (avec UI et server)
  Ca va créer un fichier R/mod_nom_du_module.R

- Pour ajouter une fonction : golem::add_fct( "helpers" ) ce qui créera un fichier R/fct_helpers.R

- Si on a des données à intégrer au package, il faut créer un dossier data-raw avec la commande : usethis::use_data_raw()

- Pour ajouter des tests : usethis::use_test( "app" ) ce qui créera le fichier de test à remplir

- Pour construire la vignette : 
  usethis::use_vignette("shinyexample")
  devtools::build_vignettes()

- A faire : 

remplir le fichier : tests/testthat/test-metadata.R

